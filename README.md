# duhm

Downgrade/Upgrade Hetzner Machines

CLI generated using [bashly](https://github.com/DannyBen/bashly)

## Why

I have an [Owncast instance](https://owncast.online) hosted on a
[Hetzner VPS](https://hetzner.cloud). To get the most out of both, I run the VPS
regularly in the lowest tier, switch it to the highest tier right before
streaming then switch back to the lowest tier after streaming.

`duhm` makes this process stupidly simple.

## Usage

Use case 1: downgrading and upgrading the machine

```
duhm upgrade server.yml
duhm downgrade server.yml
```

Use case 2: powering the machine on and off

```
duhm poweron server.yml
duhm poweroff server.yml
```

## Installation

Make sure you have `hcloud` ([repo](https://github.com/hetznercloud/cli))
installed.

For now, depending on your preference, either go with:

```
git clone https://codeberg.org/yarmo/duhm.git
cd duhm
cp duhm /usr/local/bin
```

or

```
curl https://codeberg.org/yarmo/duhm/raw/branch/main/duhm -o /usr/local/bin/duhm
```

Currently looking into packaging. Open to suggestions/PR.

## Setting up

### Hetzner/hcloud settings

Go to your Hetzner project > Security > API tokens. Generate a new API token.

On your machine, run:

```
hcloud context create my-project
```

which will ask you for the API token.

### Local settings

Create a new `server.yml` file with the following content:

```
commands:
  up: docker-compose up -d
  down: docker-compose down

ssh:
  host: 123.123.123.123
  port: 22
  user: user

vps:
  name: stream-server
  type:
    upgrade: cpx51
    downgrade: cpx11
```

## Note

Currently, the script doesn't check the current state of machines.
This may lead to scripts hanging or sometimes acting weirdly.

If sever
