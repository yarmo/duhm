echo "$(green_bold "Downgrading the Hetzner VPS")"
echo "$(green_bold "===========================")"

echo "$(blue_bold "Loading VPS settings...")"
filename=${args[filename]}
eval $(yaml_load "$filename")

echo "$(blue_bold "Executing the down commands on the VPS...")"
ssh $ssh_user@$ssh_host -p $ssh_port $commands_down

echo "$(blue_bold "Powering off the VPS...")"
hcloud server poweroff $vps_name

echo "$(blue_bold "Downgrading the VPS...")"
hcloud server change-type --keep-disk $vps_name $vps_type_downgrade

echo "$(blue_bold "Powering on the VPS...")"
hcloud server poweron $vps_name

echo "$(blue_bold "Waiting 15 seconds...")"
sleep 15

echo "$(blue_bold "Executing the up commands on the VPS...")"
ssh $ssh_user@$ssh_host -p $ssh_port $commands_up

echo "$(green_bold "Downgrade successful!")"
