echo "$(green_bold "Powering off the Hetzner VPS")"
echo "$(green_bold "============================")"

echo "$(blue_bold "Loading VPS settings...")"
filename=${args[filename]}
eval $(yaml_load "$filename")

echo "$(blue_bold "Executing the down commands on the VPS...")"
ssh $ssh_user@$ssh_host -p $ssh_port $commands_down

echo "$(blue_bold "Powering off the VPS...")"
hcloud server poweroff $vps_name

echo "$(green_bold "Power off successful!")"
