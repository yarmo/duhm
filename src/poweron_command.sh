echo "$(green_bold "Powering on the Hetzner VPS")"
echo "$(green_bold "===========================")"

echo "$(blue_bold "Loading VPS settings...")"
filename=${args[filename]}
eval $(yaml_load "$filename")

echo "$(blue_bold "Powering on the VPS...")"
hcloud server poweron $vps_name

echo "$(blue_bold "Waiting 15 seconds...")"
sleep 15

echo "$(blue_bold "Executing the up commands on the VPS...")"
ssh $ssh_user@$ssh_host -p $ssh_port $commands_up

echo "$(green_bold "Power on successful!")"
